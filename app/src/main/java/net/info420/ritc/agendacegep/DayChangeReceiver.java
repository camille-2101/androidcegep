package net.info420.ritc.agendacegep;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

//Redémarre le service de notification quand on change de journée

public class DayChangeReceiver extends BroadcastReceiver {

    private static final String TAG = "DayChangeReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        Accueil accueil = new Accueil();

        accueil.onResume();

        context.startService(new Intent(context, NotificationAgendaService.class));

        Log.d(TAG, "onReceive: Alarmes enregistrées");
    }
}
