package net.info420.ritc.agendacegep;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

//Content provider personnalisé de mon application

//Source: https://guides.codepath.com/android/Creating-Content-Providers

public class AppProvider extends ContentProvider {
    private static final String TAG = "AppProvider";

    //IDS pour le UriMatcher

    private static final int USER = 100;
    private static final int USER_ID = 101;

    private static final int CLASS = 200;
    private static final int CLASS_ID = 201;

    private static final int DEVOIR = 300;
    private static final int DEVOIR_ID = 301;

    private static final int EXAMEN = 400;
    private static final int EXAMEN_ID = 401;

    private static final UriMatcher sUriMatcher = buildUriMatcher();
    private DBHelper sDBHelper;

    public AppProvider() {
    }

    @Override
    public boolean onCreate() {
        sDBHelper = new DBHelper(getContext());

        Log.d(TAG, "onCreate: ");

        return true;
    }

    public static UriMatcher buildUriMatcher() {
        String content = Agenda.AUTHORITY;

        UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        matcher.addURI(content, Agenda.PATH_USER, USER);
        matcher.addURI(content, Agenda.PATH_USER + "/#", USER_ID);
        matcher.addURI(content, Agenda.PATH_CLASS, CLASS);
        matcher.addURI(content, Agenda.PATH_CLASS + "/#", CLASS_ID);
        matcher.addURI(content, Agenda.PATH_DEVOIR, DEVOIR);
        matcher.addURI(content, Agenda.PATH_DEVOIR + "/#", DEVOIR_ID);
        matcher.addURI(content, Agenda.PATH_EXAMEN, EXAMEN);
        matcher.addURI(content, Agenda.PATH_EXAMEN + "/#", EXAMEN_ID);

        return matcher;
    }


    //Non utilisé pour cette première version de l'apk. Peut servir à l'avenir
    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        switch (sUriMatcher.match(uri)) {
            case USER:
                return Agenda.UserEntry.CONTENT_TYPE;
            case USER_ID:
                return Agenda.UserEntry.CONTENT_ITEM_TYPE;
            case CLASS:
                return Agenda.ClassEntry.CONTENT_TYPE;
            case  CLASS_ID:
                return Agenda.ClassEntry.CONTENT_ITEM_TYPE;
            case DEVOIR:
                return  Agenda.DevoirEntry.CONTENT_TYPE;
            case DEVOIR_ID:
                return Agenda.DevoirEntry.CONTENT_ITEM_TYPE;
            case EXAMEN:
                return Agenda.ExamenEntry.CONTENT_TYPE;
            case EXAMEN_ID:
                return Agenda.ExamenEntry.CONTENT_ITEM_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }


    //S'occupe des requetes faite à la base de données Sqlite
    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection,
                        @Nullable String[] selectionArgs, @Nullable String sortOrder) {

        Cursor cursor;
        switch (sUriMatcher.match(uri)){
            case USER:
                final SQLiteDatabase db = sDBHelper.getWritableDatabase();
                cursor = db.query(
                        Agenda.UserEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                        );
                break;
            case USER_ID:
                final SQLiteDatabase db1 = sDBHelper.getWritableDatabase();
                long _id = ContentUris.parseId(uri);
                cursor = db1.query(
                        Agenda.UserEntry.TABLE_NAME,
                        projection,
                        Agenda.UserEntry._ID + " = ?",
                        new String[]{String.valueOf(_id)},
                        null,
                        null,
                        sortOrder
                );
                break;
            case CLASS:
                final SQLiteDatabase db2 = sDBHelper.getWritableDatabase();
                cursor = db2.query(
                        Agenda.ClassEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                        );
                break;
            case CLASS_ID:
                final SQLiteDatabase db3 = sDBHelper.getWritableDatabase();
                long _id1 = ContentUris.parseId(uri);
                cursor = db3.query(
                        Agenda.ClassEntry.TABLE_NAME,
                        projection,
                        Agenda.ClassEntry._ID + " = ?",
                        new String[]{String.valueOf(_id1)},
                        null,
                        null,
                        sortOrder
                );
                break;
            case DEVOIR:
                final SQLiteDatabase db4 = sDBHelper.getWritableDatabase();
                cursor = db4.query(
                        Agenda.DevoirEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            case DEVOIR_ID:
                final SQLiteDatabase db5 = sDBHelper.getWritableDatabase();
                long _id2 = ContentUris.parseId(uri);
                cursor = db5.query(
                        Agenda.DevoirEntry.TABLE_NAME,
                        projection,
                        Agenda.DevoirEntry._ID + " = ?",
                        new String[]{String.valueOf(_id2)},
                        null,
                        null,
                        sortOrder
                );
                break;
            case EXAMEN:
                final SQLiteDatabase db6 = sDBHelper.getWritableDatabase();
                cursor = db6.query(
                        Agenda.ExamenEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            case EXAMEN_ID:
                final SQLiteDatabase db7 = sDBHelper.getWritableDatabase();
                long _id3 = ContentUris.parseId(uri);
                cursor = db7.query(
                        Agenda.ExamenEntry.TABLE_NAME,
                        projection,
                        Agenda.ExamenEntry._ID + " = ?",
                        new String[]{String.valueOf(_id3)},
                        null,
                        null,
                        sortOrder
                );
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        Log.d(TAG, "query: ");
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }


    //Utilisé pour ajouter usager, classe, devoirs et examen.
    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        long _id;
        Uri returnUri;

        switch (sUriMatcher.match(uri)){
            case USER:
                final SQLiteDatabase db = sDBHelper.getWritableDatabase();
                _id = db.insert(Agenda.UserEntry.TABLE_NAME, null, values);
                if(_id > 0){
                    returnUri =  Agenda.UserEntry.buildUserUri(_id);
                } else{
                    throw new UnsupportedOperationException("Unable to insert rows into: " + uri);
                }
                break;
            case CLASS:
                final SQLiteDatabase db1 = sDBHelper.getWritableDatabase();
                _id = db1.insert(Agenda.ClassEntry.TABLE_NAME, null, values);
                if(_id > 0){
                    returnUri =  Agenda.ClassEntry.buildClassUri(_id);
                } else{
                    throw new UnsupportedOperationException("Unable to insert rows into: " + uri);
                }
                break;
            case DEVOIR:
                final SQLiteDatabase db2 = sDBHelper.getWritableDatabase();
                _id = db2.insert(Agenda.DevoirEntry.TABLE_NAME, null, values);
                if(_id > 0){
                    returnUri =  Agenda.DevoirEntry.buildDevoirUri(_id);
                } else{
                    throw new UnsupportedOperationException("Unable to insert rows into: " + uri);
                }
                break;
            case EXAMEN:
                final SQLiteDatabase db3 = sDBHelper.getWritableDatabase();
                _id = db3.insert(Agenda.ExamenEntry.TABLE_NAME, null, values);
                if(_id > 0){
                    returnUri =  Agenda.ExamenEntry.buildExamenUri(_id);
                } else{
                    throw new UnsupportedOperationException("Unable to insert rows into: " + uri);
                }
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        Log.d(TAG, "insert: ");
        getContext().getContentResolver().notifyChange(uri, null);
        return returnUri;
    }


    //Non utilisé pour cette première version de l'apk. Peut servir à l'avenir
    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection,
                      @Nullable String[] selectionArgs) {
        int rows; // Number of rows effected

        switch(sUriMatcher.match(uri)){
            case USER:
                final SQLiteDatabase db = sDBHelper.getWritableDatabase();
                rows = db.delete(Agenda.UserEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case CLASS:
                final SQLiteDatabase db1 = sDBHelper.getWritableDatabase();
                rows = db1.delete(Agenda.ClassEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case DEVOIR:
                final SQLiteDatabase db2 = sDBHelper.getWritableDatabase();
                rows = db2.delete(Agenda.DevoirEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case EXAMEN:
                final SQLiteDatabase db3 = sDBHelper.getWritableDatabase();
                rows = db3.delete(Agenda.ExamenEntry.TABLE_NAME, selection, selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        if(selection == null || rows != 0){
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return rows;
    }


    //Non utilisé pour cette première version de l'apk. Peut servir à l'avenir
    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection,
                      @Nullable String[] selectionArgs) {
        int rows;

        switch(sUriMatcher.match(uri)){
            case USER:
                final SQLiteDatabase db = sDBHelper.getWritableDatabase();
                rows = db.update(Agenda.UserEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            case CLASS:
                final SQLiteDatabase db1 = sDBHelper.getWritableDatabase();
                rows = db1.update(Agenda.ClassEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            case DEVOIR:
                final SQLiteDatabase db2 = sDBHelper.getWritableDatabase();
                rows = db2.update(Agenda.DevoirEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            case EXAMEN:
                final SQLiteDatabase db3 = sDBHelper.getWritableDatabase();
                rows = db3.update(Agenda.ExamenEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        if(rows != 0){
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return rows;

    }


    //S'occupe de la création, et "upgrade" de la BD
    public class DBHelper extends SQLiteOpenHelper {
        private static final int DB_VERSION = 1;
        private static final String DB_NAME = "agenda.db";

        public DBHelper(Context context) {
            super(context, DB_NAME, null, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            addTables(db);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int i, int i1) {
            String sql = "DROP TABLE IF EXISTS " + Agenda.UserEntry.TABLE_NAME;
            db.execSQL(sql);

            sql = "DROP TABLE IF EXISTS " + Agenda.ClassEntry.TABLE_NAME;
            db.execSQL(sql);

            sql = "DROP TABLE IF EXISTS " + Agenda.DevoirEntry.TABLE_NAME;
            db.execSQL(sql);

            sql = "DROP TABLE IF EXISTS " + Agenda.ExamenEntry.TABLE_NAME;
            db.execSQL(sql);

            onCreate(db);
        }

        private void addTables(SQLiteDatabase db) {
            db.execSQL(
                    "CREATE TABLE " + Agenda.UserEntry.TABLE_NAME + " (" +
                            Agenda.UserEntry._ID + " INTEGER PRIMARY KEY, " +
                            Agenda.UserEntry.COLUMN_USERNAME + " TEXT NOT NULL, " +
                            Agenda.UserEntry.COLUMN_PASSWORD + " TEXT NOT NULL);"
            );


            db.execSQL(
                    "CREATE TABLE " + Agenda.ClassEntry.TABLE_NAME + " (" +
                            Agenda.ClassEntry._ID + " INTEGER PRIMARY KEY, " +
                            Agenda.ClassEntry.COLUMN_CLASSE + " TEXT NOT NULL, " +
                            Agenda.ClassEntry.COLUMN_PROF + " TEXT NOT NULL, " +
                            Agenda.ClassEntry.COLUMN_DATE_DEBUT + " TEXT NOT NULL, " +
                            Agenda.ClassEntry.COLUMN_DATE_FIN + " TEXT NOT NULL, " +
                            Agenda.ClassEntry.COLUMN_HEURE_DEBUT + " TEXT NOT NULL, " +
                            Agenda.ClassEntry.COLUMN_HEURE_FIN + " TEXT NOT NULL, " +
                            Agenda.ClassEntry.COLUMN_DAY + " TEXT NOT NULL, " +
//                            Agenda.ClassEntry.COLUMN_FK_USER + " INTEGER REFERENCES " +
//                            Agenda.UserEntry.TABLE_NAME + ");"
                            Agenda.ClassEntry.COLUMN_FK_USER + " INTEGER, " +
                            "FOREIGN KEY(" + Agenda.ClassEntry.COLUMN_FK_USER + ") REFERENCES " +
                            Agenda.UserEntry.TABLE_NAME + "(" + Agenda.UserEntry._ID + "));"

            );


            db.execSQL(
                    "CREATE TABLE " + Agenda.DevoirEntry.TABLE_NAME + " (" +
                            Agenda.DevoirEntry._ID + " INTEGER PRIMARY KEY, " +
                            Agenda.DevoirEntry.COLUMN_DEVOIR + " TEXT NOT NULL, " +
                            Agenda.DevoirEntry.COLUMN_DESCRIPTION + " TEXT NOT NULL, " +
                            Agenda.DevoirEntry.COLUMN_DATE_REMISE + " TEXT NOT NULL, " +
                            Agenda.DevoirEntry.COLUMN_FK_USER + " INTEGER, " +
                            " FOREIGN KEY (" + Agenda.DevoirEntry.COLUMN_FK_USER + ") REFERENCES " +
                            Agenda.UserEntry.TABLE_NAME + "(" + Agenda.UserEntry._ID + "));"

            );


            db.execSQL(
                    "CREATE TABLE " + Agenda.ExamenEntry.TABLE_NAME + " (" +
                            Agenda.ExamenEntry._ID + " INTEGER PRIMARY KEY, " +
                            Agenda.ExamenEntry.COLUMN_EXAMEN + " TEXT NOT NULL, " +
                            Agenda.ExamenEntry.COLUMN_PROFESSEUR + " TEXT NOT NULL, " +
                            Agenda.ExamenEntry.COLUMN_HEURE_DEBUT + " TEXT NOT NULL, " +
                            Agenda.ExamenEntry.COLUMN_HEURE_FIN + " TEXT NOT NULL, " +
                            Agenda.ExamenEntry.COLUMN_DATE + " TEXT NOT NULL, " +
                            Agenda.DevoirEntry.COLUMN_FK_USER + " INTEGER, " +
                            " FOREIGN KEY (" + Agenda.ExamenEntry.COLUMN_FK_USER + ") REFERENCES " +
                            Agenda.UserEntry.TABLE_NAME + "(" + Agenda.UserEntry._ID + "));"

            );
        }
    }
}
