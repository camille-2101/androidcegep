package net.info420.ritc.agendacegep;

import android.content.ContentValues;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

//Activité qui permet la création d'un utilisateur pour pouvoir se connecter

public class CreerUtilisateur extends AppCompatActivity implements View.OnClickListener {
    private Toolbar toolbar;
    private Button creerUtilisateur;
    private EditText nomUtilisateur, mdp, mdp2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creer_utilisateur);

        initToolbar();

        initUI();
    }

    private void initUI() {
        creerUtilisateur = (Button)findViewById(R.id.buttonCreerUser);
        creerUtilisateur.setOnClickListener(this);

        nomUtilisateur = (EditText)findViewById(R.id.editTextCreerUserUsername);
        mdp = (EditText)findViewById(R.id.editTextCreerUserMotPasse);
        mdp2 = (EditText)findViewById(R.id.editTextCreerUserConfirmerMotPasse);
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        toolbar.setTitle(R.string.app_name);
        toolbar.setNavigationIcon(R.mipmap.ic_launcher);
    }

    @Override
    public void onClick(View view) {
        ContentValues values = new ContentValues();
        values.put(Agenda.UserEntry.COLUMN_USERNAME, nomUtilisateur.getText().toString());
        values.put(Agenda.UserEntry.COLUMN_PASSWORD, mdp.getText().toString());
        boolean erreur = false;


        //SI valide, on crée l'utilisateur
        if (valider()) {
            try {
                getApplicationContext().getContentResolver().insert(Agenda.UserEntry.CONTENT_URI, values);
            } catch (Exception e) {
                erreur = true;
            }

            if (erreur) {
                Toast.makeText(this, "Une erreur s'est produite. Impossible de créer l'utilisateur.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "Utilisateur créé avec succès.", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(this, "Impossible de créer l'utilisateur. Vérifiez que vos champs soient remplis " +
                    "et que vos deux mot de passes concordent.", Toast.LENGTH_LONG).show();
        }
    }


    //Validation
    private boolean valider() {
        if (nomUtilisateur.getText().toString().isEmpty()) {
            return  false;
        }
        if (mdp.getText().toString().isEmpty()) {
            return false;
        }
        if (mdp2.getText().toString().isEmpty()) {
            return false;
        }
        if (!mdp.getText().toString().equals(mdp2.getText().toString())){
            return false;
        }
        return true;
    }
}
