package net.info420.ritc.agendacegep;

import android.app.DatePickerDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Calendar;


//Fragment qui ajoute des devoirs à l'horaire

public class AjouterDevoir extends Fragment implements View.OnClickListener {
    Context context;
    private static final String TAG = "AjouterDevoir";

    private Calendar c = Calendar.getInstance();
    private Button boutonAjouterDevoir, boutonDateRemise;
    private EditText editTextDateRemise, editTextDevoir, editTextDescription;
    private int dateDebutJour = c.get(Calendar.DAY_OF_MONTH), dateDebutMois = c.get(Calendar.MONTH), dateDebutAnnee = c.get(Calendar.YEAR);


    public AjouterDevoir() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d(TAG, "onCreate: ");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        Button boutonAjouter;

        view = inflater.inflate(R.layout.fragment_ajouter_devoir, container, false);

        boutonDateRemise = (Button) view.findViewById(R.id.boutonDateRemiseDevoir);
        boutonDateRemise.setOnClickListener(this);

        boutonAjouterDevoir = (Button) view.findViewById(R.id.boutonAjouterDevoir);
        boutonAjouterDevoir.setOnClickListener(this);

        editTextDateRemise = (EditText) view.findViewById(R.id.editTextDateRemiseDevoir);
        editTextDateRemise.setKeyListener(null);

        editTextDevoir = (EditText) view.findViewById(R.id.editTextNomDevoir);

        editTextDescription = (EditText) view.findViewById(R.id.editTextDescriptionDevoir);

        Log.d(TAG, "onCreateView: ");

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        this.context = context;

        Log.d(TAG, "onAttach: ");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(TAG, "onDetach: ");
    }


    //Quand les boutons sont cliqués
    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.boutonDateRemiseDevoir:
                DatePickerDialog debut = new DatePickerDialog(context,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                editTextDateRemise.setText(String.format("%04d-%02d-%02d", year, monthOfYear + 1, dayOfMonth));
                            }
                        }, dateDebutAnnee, dateDebutMois, dateDebutJour);
                debut.show();
                break;
            case R.id.boutonAjouterDevoir:
                if (valider()) {
                    ajouterDevoir();
                    context.startService(new Intent(context, NotificationAgendaService.class));
                } else {
                    Toast.makeText(context, "Il ne faut laisser rien de vide !", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }


    //Requete faite au content provider pour ajouter un devoir
    private void ajouterDevoir() {
        boolean erreur = false;

        ContentValues values = new ContentValues();
        values.put(Agenda.DevoirEntry.COLUMN_DEVOIR, editTextDevoir.getText().toString());
        values.put(Agenda.DevoirEntry.COLUMN_DESCRIPTION, editTextDescription.getText().toString());
        values.put(Agenda.DevoirEntry.COLUMN_DATE_REMISE, editTextDateRemise.getText().toString());
        String[] selection = {AgendaApplication.getUser(), AgendaApplication.getPassword()};
        String[] columns = {Agenda.UserEntry._ID};

        Cursor user = getContext().getContentResolver().query(Agenda.UserEntry.CONTENT_URI,
                columns,
                Agenda.UserEntry.COLUMN_USERNAME + " = ? AND " + Agenda.UserEntry.COLUMN_PASSWORD + " = ?",
                selection,
                null);

        if (user.moveToFirst())
            values.put(Agenda.DevoirEntry.COLUMN_FK_USER, user.getString(user.getColumnIndex(Agenda.UserEntry._ID)));
        user.close();

        try {
            getContext().getContentResolver().insert(Agenda.DevoirEntry.CONTENT_URI, values);
        } catch (SQLException e) {
            erreur = true;
        }

        if (erreur) {
            Toast.makeText(context, "Une erreur s'est produite. Echec de l'insertion de devoire dans la base de données.", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(context, "Insertion de devoir dans la base de données avec succès !", Toast.LENGTH_SHORT).show();
        }
    }

    //Validation
    private boolean valider() {

        if (editTextDevoir.getText().toString().isEmpty()){
            return false;
        }
        if (editTextDescription.getText().toString().isEmpty()) {
            return false;
        }
        if (editTextDateRemise.getText().toString().isEmpty()) {
            return false;
        }

        return true;
    }
}
