package net.info420.ritc.agendacegep;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import java.util.Calendar;

import static java.lang.Boolean.FALSE;

/**
 * Created by ritc on 2017-03-23.
 *
 * Dans cette application, la plupart des sources proviennent de l'application Yamba créée en groupe en classe
 */




//Classe regroupant les variables les plus globales
public class AgendaApplication extends Application {
    private final static String TAG = "AgendaApplication";

    private static boolean ajouterClasse = FALSE;
    private static boolean ajouterExamen = FALSE;
    private static boolean ajouterDevoir = FALSE;

    private static String user;
    private static String password;

    private static SharedPreferences preferences;
    private static Calendar c = Calendar.getInstance();

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate: ");

        preferences = PreferenceManager.getDefaultSharedPreferences(this);
    }

    public static void setAjouterClasse(boolean ajouterClasse) {
        AgendaApplication.ajouterClasse = ajouterClasse;
    }

    public static void setAjouterExamen(boolean ajouterExamen) {
        AgendaApplication.ajouterExamen = ajouterExamen;
    }

    public static void setAjouterDevoir(boolean ajouterHoraire) {
        AgendaApplication.ajouterDevoir = ajouterHoraire;
    }

    public static void setUser(String user) {
        preferences.edit().putString("user", user).apply();
        AgendaApplication.user = preferences.getString("user", null);
    }

    public static void setPassword(String password) {
        preferences.edit().putString("password", password).apply();
        AgendaApplication.password = preferences.getString("password", null);
    }

    public static boolean getAjouterClasse() {
        return ajouterClasse;
    }

    public static boolean getAjouterExamen() {
        return ajouterExamen;
    }

    public static boolean getAjouterDevoir() {
        return ajouterDevoir;
    }

    public static String getUser() {
        return user;
    }

    public static String getPassword() {
        return password;
    }

    public static SharedPreferences getPreferences() {
        return preferences;
    }

    public static String getJour() {
        int jour = c.get(Calendar.DAY_OF_WEEK);
        String jour1;

        switch (jour) {
            case Calendar.SUNDAY:
                jour1 = "Dimanche";
                break;
            case Calendar.MONDAY:
                jour1 = "Lundi";
                break;
            case Calendar.TUESDAY:
                jour1 = "Mardi";
                break;
            case Calendar.WEDNESDAY:
                jour1 = "Mercredi";
                break;
            case Calendar.THURSDAY:
                jour1 = "Jeudi";
                break;
            case Calendar.FRIDAY:
                jour1 = "Vendredi";
                break;
            case Calendar.SATURDAY:
                jour1 = "Samedi";
                break;
            default:
                jour1 = "Erreur";
                break;
        }

        preferences.edit().putString("jour", jour1).apply();
        return  preferences.getString("jour", null);
    }

    public static String getDate () {
        int jour = c.get(Calendar.DAY_OF_MONTH);
        int mois = c.get(Calendar.MONTH) + 1;
        int annee = c.get(Calendar.YEAR);

        String date = String.format("%04d-%02d-%02d", annee, mois, jour);

        return date;
    }
}
