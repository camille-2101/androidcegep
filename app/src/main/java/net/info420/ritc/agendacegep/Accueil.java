package net.info420.ritc.agendacegep;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.Calendar;

public class Accueil extends AppCompatActivity {
    static final String TAG = "Accueil";

    ListView outputHoraire, outputExamens, outputDevoirs;
    private Toolbar toolbar;
    private Cursor cursor;
    SimpleCursorAdapter adapter;
    static final String[] from = {Agenda.ClassEntry.COLUMN_CLASSE, Agenda.ClassEntry.COLUMN_HEURE_DEBUT, Agenda.ClassEntry.COLUMN_HEURE_FIN};
    static final int[] to = {R.id.classe, R.id.heure_debut, R.id.heure_fin};

    static final String[] from1 = {Agenda.ExamenEntry.COLUMN_EXAMEN, Agenda.ExamenEntry.COLUMN_DATE, Agenda.ExamenEntry.COLUMN_HEURE_DEBUT};

    static final String[] from2 = {Agenda.DevoirEntry.COLUMN_DEVOIR, Agenda.DevoirEntry.COLUMN_DESCRIPTION, Agenda.DevoirEntry.COLUMN_DATE_REMISE};
    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_horaire);

        preferences = AgendaApplication.getPreferences();

        initToolbar();
        initUI();

        getCalendars(); //Pour tester la permission dangereuse
    }

    //Pour initialiser la barre d'outils
    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        toolbar.setTitle(R.string.app_name);
        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.couleurTitreToolbar));

        toolbar.setNavigationIcon(R.mipmap.ic_launcher);

        if (preferences.getString("theme_pref", "").equals("Rouge") || preferences.getString("theme_pref", "").equals("Red")) {
            toolbar.setBackgroundColor(getResources().getColor(R.color.rouge));
        } else {
            toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        Log.d(TAG, "initToolbar: ");
    }

    //Initialiser le layout
    private void initUI() {

        outputHoraire = (ListView) findViewById(R.id.outputHoraire);
        String[] selection = {AgendaApplication.getUser(), AgendaApplication.getPassword()};
        String[] selection2 = {AgendaApplication.getUser(), AgendaApplication.getPassword(), AgendaApplication.getJour()};
        //Affiche les classes dans un listview
        cursor = getApplicationContext().getContentResolver().query
                (Agenda.ClassEntry.CONTENT_URI,
                        null,
                        Agenda.ClassEntry.COLUMN_FK_USER + " = (SELECT " + Agenda.UserEntry._ID +
                                " FROM " + Agenda.UserEntry.TABLE_NAME + " WHERE " + Agenda.UserEntry.COLUMN_USERNAME + " = ? AND " +
                                Agenda.UserEntry.COLUMN_PASSWORD + " = ?) AND DATE(" + Agenda.ClassEntry.COLUMN_DATE_DEBUT + ")" +
                                " <= DATE('now', 'localtime') AND DATE(" + Agenda.ClassEntry.COLUMN_DATE_FIN + ") >= DATE('now','localtime') " +
                                "AND " + Agenda.ClassEntry.COLUMN_DAY + " = ?",
                        selection2,
                        null
                );


        adapter = new SimpleCursorAdapter(this, R.layout.row, cursor, from, to, 0);
        outputHoraire.setAdapter(adapter);

        //Affiche les examen dans un listview
        outputExamens = (ListView) findViewById(R.id.outputExamens);
        cursor = getApplicationContext().getContentResolver().query
                (Agenda.ExamenEntry.CONTENT_URI,
                        null,
                        Agenda.ExamenEntry.COLUMN_FK_USER + " = (SELECT " + Agenda.UserEntry._ID +
                                " FROM " + Agenda.UserEntry.TABLE_NAME + " WHERE " + Agenda.UserEntry.COLUMN_USERNAME + " = ? AND " +
                                Agenda.UserEntry.COLUMN_PASSWORD + " = ?) AND DATE(" + Agenda.ExamenEntry.COLUMN_DATE + ") >= " +
                                "DATE('now','localtime')",
                        selection,
                        null
                );
        adapter = new SimpleCursorAdapter(this, R.layout.row, cursor, from1, to, 0);
        outputExamens.setAdapter(adapter);

        //Affiche les devoirs dans un listview
        outputDevoirs = (ListView) findViewById(R.id.outputDevoirs);
        cursor = getApplicationContext().getContentResolver().query
                (Agenda.DevoirEntry.CONTENT_URI,
                        null,
                        Agenda.DevoirEntry.COLUMN_FK_USER + " = (SELECT " + Agenda.UserEntry._ID +
                                " FROM " + Agenda.UserEntry.TABLE_NAME + " WHERE " + Agenda.UserEntry.COLUMN_USERNAME + " = ? AND " +
                                Agenda.UserEntry.COLUMN_PASSWORD + " = ?) AND DATE(" + Agenda.DevoirEntry.COLUMN_DATE_REMISE + ") >= " +
                                "DATE('now','localtime')",
                        selection,
                        null
                );
        adapter = new SimpleCursorAdapter(this, R.layout.row, cursor, from2, to, 0);
        outputDevoirs.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        initUI();
        initToolbar();
        Log.d(TAG, "onResume: ");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);

        Log.d(TAG, "onCreateOptionsMenu: ");

        return true;
    }

    //Quand on sélectionne un élément du menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Intent intentModifHoraire = new Intent(this, ModifierHoraireActivity.class);
        Intent intentPreferences = new Intent(this, PrefActivity.class);

        switch (item.getItemId()) {
            case R.id.itemMenuAjouterClasse:
                Log.d(TAG, "onMenuItemClick: ajouter classe");
                AgendaApplication.setAjouterClasse(true);
                AgendaApplication.setAjouterExamen(false);
                AgendaApplication.setAjouterDevoir(false);
                startActivity(intentModifHoraire);
                return true;
            case R.id.itemMenuAjouterDevoir:
                Log.d(TAG, "onMenuItemClick: ajouter devoir");
                AgendaApplication.setAjouterClasse(false);
                AgendaApplication.setAjouterExamen(false);
                AgendaApplication.setAjouterDevoir(true);
                startActivity(intentModifHoraire);
                return true;
            case R.id.itemMenuAjouterExamen:
                Log.d(TAG, "onMenuItemClick: ajouter examen");
                AgendaApplication.setAjouterClasse(false);
                AgendaApplication.setAjouterExamen(true);
                AgendaApplication.setAjouterDevoir(false);
                startActivity(intentModifHoraire);
                return true;
            case R.id.itemMenuPréférences:
                Log.d(TAG, "onMenuItemClick: préférences");
                startActivity(intentPreferences);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //http://www.techrepublic.com/blog/software-engineer/programming-with-the-android-40-calendar-api-the-good-the-bad-and-the-ugly/
    //SEULEMENT pour tester la permission.

    private void getCalendars() {
        String[] projection = new String[]{"_id", "name"};


        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_CALENDAR)
                != PackageManager.PERMISSION_GRANTED) {


            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_CALENDAR}, 1);

        }
    }

    //Pour la gestion de permissions
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        final String[] COLS = new String[]
                {CalendarContract.Events.TITLE, CalendarContract.Events.DTSTART};
        switch (requestCode) {
            case 1: {

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    try {
                        Cursor managedCursor = getContentResolver().query(CalendarContract.Events.CONTENT_URI, COLS, null, null, null);

                        managedCursor.close();
                    } catch (SecurityException e) {
                        e.printStackTrace();
                    }

                } else {

                    // permission denied
                }
                return;
            }
        }
    }
}