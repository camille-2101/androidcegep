package net.info420.ritc.agendacegep;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static net.info420.ritc.agendacegep.R.id.editTextDateExamen;


//Fragment qui ajoute un examen à l'horaire
public class AjouterExamen extends Fragment implements View.OnClickListener {
    Context context;
    private static final String TAG = "AjouterExamen";

    private Calendar c = Calendar.getInstance();
    private Button boutonAjouterExamen, boutonHeureDebut, boutonHeureFin, boutonDate;
    private EditText editTextDate, editTextHeureDebut, editTextHeureFin, editTextMatiere, editTextProf;
    private int dateDebutJour = c.get(Calendar.DAY_OF_MONTH), dateDebutMois = c.get(Calendar.MONTH), dateDebutAnnee = c.get(Calendar.YEAR),
            heureFinHeure = c.get(Calendar.HOUR_OF_DAY), heureFinMinute = c.get(Calendar.MINUTE);

    private SimpleDateFormat heureFormatter;

    public AjouterExamen() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d(TAG, "onCreate: ");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;

        view = inflater.inflate(R.layout.fragment_ajouter_examen, container, false);

        boutonDate = (Button) view.findViewById(R.id.boutonDateExamen);
        boutonDate.setOnClickListener(this);

        boutonAjouterExamen = (Button) view.findViewById(R.id.boutonAjouterExamen);
        boutonAjouterExamen.setOnClickListener(this);

        boutonHeureDebut = (Button) view.findViewById(R.id.boutonHeureDebutExamen);
        boutonHeureDebut.setOnClickListener(this);

        boutonHeureFin = (Button) view.findViewById(R.id.boutonHeureFinExamen);
        boutonHeureFin.setOnClickListener(this);

        editTextDate = (EditText) view.findViewById(editTextDateExamen);
        editTextDate.setKeyListener(null);

        editTextHeureDebut = (EditText) view.findViewById(R.id.editTextHeureDebutExamen);
        editTextHeureDebut.setKeyListener(null);

        editTextHeureFin = (EditText) view.findViewById(R.id.editTextHeureFinExamen);
        editTextHeureFin.setKeyListener(null);

        editTextMatiere = (EditText) view.findViewById(R.id.editTextNomExamen);

        editTextProf = (EditText) view.findViewById(R.id.editTextProfExamen);

        Log.d(TAG, "onCreateView: ");

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        this.context = context;

        Log.d(TAG, "onAttach: ");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(TAG, "onDetach: ");
    }


    //Quand un bouton est cliqué
    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.boutonDateExamen:
                DatePickerDialog debut = new DatePickerDialog(context,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                editTextDate.setText(String.format("%04d-%02d-%02d", year, monthOfYear + 1, dayOfMonth));
                            }
                        }, dateDebutAnnee, dateDebutMois, dateDebutJour);
                debut.show();
                break;
            case R.id.boutonHeureDebutExamen:
                TimePickerDialog debut1 = new TimePickerDialog(context,
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                editTextHeureDebut.setText(String.format("%02d:%02d", hourOfDay, minute));
                            }
                        }, heureFinHeure, heureFinMinute, true);
                debut1.show();
                break;
            case R.id.boutonHeureFinExamen:
                TimePickerDialog fin = new TimePickerDialog(context,
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                editTextHeureFin.setText(String.format("%02d:%02d", hourOfDay, minute));
                            }
                        }, heureFinHeure, heureFinMinute, true);
                fin.show();
                break;
            case R.id.boutonAjouterExamen:
                if (valider()) {
                    ajouterExamen();
                    context.startService(new Intent(context, NotificationAgendaService.class));
                } else {
                    Toast.makeText(context, "Il y a une erreur. Vérifiez que tous vos champs soient remplis et que vos heures soient logiques.",
                            Toast.LENGTH_LONG).show();
                }
                break;
        }
    }


    //Requete faite au Content provider pour ajouter l'examen
    private void ajouterExamen() {
        boolean erreur = false;

        ContentValues values = new ContentValues();
        values.put(Agenda.ExamenEntry.COLUMN_EXAMEN, editTextMatiere.getText().toString());
        values.put(Agenda.ExamenEntry.COLUMN_PROFESSEUR, editTextProf.getText().toString());
        values.put(Agenda.ExamenEntry.COLUMN_DATE, editTextDate.getText().toString());
        values.put(Agenda.ExamenEntry.COLUMN_HEURE_DEBUT, editTextHeureDebut.getText().toString());
        values.put(Agenda.ExamenEntry.COLUMN_HEURE_FIN, editTextHeureFin.getText().toString());
        String[] selection = {AgendaApplication.getUser(), AgendaApplication.getPassword()};
        String[] columns = {Agenda.UserEntry._ID};

        Cursor user = getContext().getContentResolver().query(Agenda.UserEntry.CONTENT_URI,
                columns,
                Agenda.UserEntry.COLUMN_USERNAME + " = ? AND " + Agenda.UserEntry.COLUMN_PASSWORD + " = ?",
                selection,
                null);

        if (user.moveToFirst())
            values.put(Agenda.ExamenEntry.COLUMN_FK_USER, user.getString(user.getColumnIndex(Agenda.UserEntry._ID)));
        user.close();

        try {
            getContext().getContentResolver().insert(Agenda.ExamenEntry.CONTENT_URI, values);
        } catch (SQLException e) {
            erreur = true;
        }

        if (erreur) {
            Toast.makeText(context, "Une erreur s'est produite. Echec de l'insertion de l'examen dans la base de données.", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(context, "Insertion de l'examen dans la base de données avec succès !", Toast.LENGTH_SHORT).show();
        }
    }


    //Validation
    private boolean valider() {
        heureFormatter = new SimpleDateFormat("HH:mm");

        if (editTextMatiere.getText().toString().isEmpty()){
            return false;
        }
        if (editTextProf.getText().toString().isEmpty()) {
            return false;
        }
        if (editTextDate.getText().toString().isEmpty()) {
            return false;
        }
        if (editTextHeureDebut.getText().toString().isEmpty()) {
            return false;
        }
        if (editTextHeureFin.getText().toString().isEmpty()) {
            return  false;
        }

        try {
            Date heureDebut = heureFormatter.parse(editTextHeureDebut.getText().toString());
            Date heureFin = heureFormatter.parse(editTextHeureFin.getText().toString());

            if(heureDebut.after(heureFin)){
                return false;
            }
        } catch (ParseException e) {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
        }
        return true;
    }
}
