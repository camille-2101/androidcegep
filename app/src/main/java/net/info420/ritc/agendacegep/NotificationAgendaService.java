package net.info420.ritc.agendacegep;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


/**
 * DESFOIS FONCTIONNE, DESFOIS NE FONCTIONNE PAS .... Par soucis de temps, le service ne notification
 * n'as pas pu être "debug" pour qu'il fonctionne à 100%
 */


//source : https://gist.github.com/BrandonSmith/6679223
public class NotificationAgendaService extends IntentService {
    private static final String TAG = "NotificationService";

    private SharedPreferences preferences = AgendaApplication.getPreferences();


    public NotificationAgendaService() {
        super("NotificationAgendaService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String[] projection = {Agenda.ClassEntry.COLUMN_CLASSE, Agenda.ClassEntry.COLUMN_HEURE_DEBUT};
        String[] selection = {preferences.getString("user", null), preferences.getString("password", null) , AgendaApplication.getJour()};
        String [] projection2 = {Agenda.ExamenEntry.COLUMN_EXAMEN, Agenda.ExamenEntry.COLUMN_DATE};
        String[] selection2 = {preferences.getString("user", null), preferences.getString("password", null)};
        String[] projection3 = {Agenda.DevoirEntry.COLUMN_DEVOIR, Agenda.DevoirEntry.COLUMN_DATE_REMISE};

        Cursor cursor = this.getContentResolver().query(Agenda.ClassEntry.CONTENT_URI,
                projection,
                Agenda.ClassEntry.COLUMN_FK_USER + " = (SELECT " + Agenda.UserEntry._ID +
                        " FROM " + Agenda.UserEntry.TABLE_NAME + " WHERE " + Agenda.UserEntry.COLUMN_USERNAME + " = ? AND " +
                        Agenda.UserEntry.COLUMN_PASSWORD + " = ?) AND DATE(" + Agenda.ClassEntry.COLUMN_DATE_DEBUT + ")" +
                        " <= DATE('now', 'localtime') AND DATE(" + Agenda.ClassEntry.COLUMN_DATE_FIN + ") >= DATE('now','localtime') " +
                        "AND " + Agenda.ClassEntry.COLUMN_DAY + " = ?",
                selection,
                null);

        while (cursor.moveToNext()) {
            try{
                Calendar c = Calendar.getInstance();

                String contenu = getContentNotification(cursor.getString(cursor.getColumnIndex(Agenda.ClassEntry.COLUMN_CLASSE)), 1, 0);

                String heureDebut = cursor.getString(cursor.getColumnIndex(Agenda.ClassEntry.COLUMN_HEURE_DEBUT));
                int mois = c.get(Calendar.MONTH);
                int jour = c.get(Calendar.DAY_OF_MONTH);
                int annee = c.get(Calendar.YEAR);

                Date heure = new SimpleDateFormat("HH:mm").parse(heureDebut);

                c.setTimeInMillis(heure.getTime());
                c.set(Calendar.MONTH, mois);
                c.set(Calendar.DAY_OF_MONTH, jour);
                c.set(Calendar.YEAR, annee);
                c.set(Calendar.SECOND, 0);
                c.set(Calendar.MILLISECOND, 0);

                c.add(Calendar.MINUTE, Integer.parseInt(preferences.getString("preference_delais", null)) * -1);

                if (c.get(Calendar.MINUTE) < 0) {
                    c.set(Calendar.HOUR_OF_DAY, Calendar.HOUR_OF_DAY - 1);

                    if (c.get(Calendar.HOUR_OF_DAY) < 0) {
                        c.set(Calendar.HOUR_OF_DAY, 23);
                    }

                    c.set(Calendar.MINUTE, Calendar.MINUTE * -1);
                }

                Log.d(TAG, "onHandleIntent: ");

                planifierNotification(getNotification(contenu), c);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        cursor.close();

        cursor = this.getApplicationContext().getContentResolver().query
                (Agenda.ExamenEntry.CONTENT_URI,
                        projection2,
                        Agenda.ExamenEntry.COLUMN_FK_USER + " = (SELECT " + Agenda.UserEntry._ID +
                                " FROM " + Agenda.UserEntry.TABLE_NAME + " WHERE " + Agenda.UserEntry.COLUMN_USERNAME + " = ? AND " +
                                Agenda.UserEntry.COLUMN_PASSWORD + " = ?) AND DATE(" + Agenda.ExamenEntry.COLUMN_DATE + ") >= " +
                                "DATE('now','localtime')",
                        selection2,
                        null
                );

        while (cursor.moveToNext()) {
            try{
                Calendar c = Calendar.getInstance();

                String contenu = getContentNotification(cursor.getString(cursor.getColumnIndex(Agenda.ExamenEntry.COLUMN_EXAMEN)), 2, 3);

                String date = cursor.getString(cursor.getColumnIndex(Agenda.ExamenEntry.COLUMN_DATE));

                Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(date);

                c.setTimeInMillis(date1.getTime());

                c.set(Calendar.HOUR_OF_DAY, 0);
                c.set(Calendar.MINUTE, 0);
                c.set(Calendar.SECOND, 0);
                c.set(Calendar.MILLISECOND, 0);

                c.add(Calendar.DAY_OF_MONTH, -3);

                if (c.getTimeInMillis() > System.currentTimeMillis()) {
                    planifierNotification(getNotification(contenu), c);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }

            Log.d(TAG, "onHandleIntent: ");
        }

        cursor.close();

        cursor = this.getApplicationContext().getContentResolver().query
                (Agenda.DevoirEntry.CONTENT_URI,
                        projection3,
                        Agenda.DevoirEntry.COLUMN_FK_USER + " = (SELECT " + Agenda.UserEntry._ID +
                                " FROM " + Agenda.UserEntry.TABLE_NAME + " WHERE " + Agenda.UserEntry.COLUMN_USERNAME + " = ? AND " +
                                Agenda.UserEntry.COLUMN_PASSWORD + " = ?) AND DATE(" + Agenda.DevoirEntry.COLUMN_DATE_REMISE + ") >= " +
                                "DATE('now','localtime')",
                        selection2,
                        null
                );

        while (cursor.moveToNext()) {
            try{
                Calendar c = Calendar.getInstance();

                String contenu = getContentNotification(cursor.getString(cursor.getColumnIndex(Agenda.DevoirEntry.COLUMN_DEVOIR)), 3, 3);

                String date = cursor.getString(cursor.getColumnIndex(Agenda.DevoirEntry.COLUMN_DATE_REMISE));

                Date date1 = new SimpleDateFormat("YYYY-MM-dd").parse(date);

                c.setTimeInMillis(date1.getTime());

                c.set(Calendar.HOUR_OF_DAY, 0);
                c.set(Calendar.MINUTE, 0);
                c.set(Calendar.SECOND, 0);
                c.set(Calendar.MILLISECOND, 0);

                c.add(Calendar.DAY_OF_MONTH, -3);

                if (c.getTimeInMillis() > System.currentTimeMillis()) {
                    planifierNotification(getNotification(contenu), c);
                }

                Log.d(TAG, "onHandleIntent: ");
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        Log.d(TAG, "onHandleIntent: ");
    }

    private void planifierNotification(Notification notification, Calendar cal) {
        Intent notificationIntent = new Intent(this, NotificationPublisher.class);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION_ID, 1);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION, notification);
        PendingIntent intent = PendingIntent.getBroadcast(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager am = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        am.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), intent);

        Log.d(TAG, "planifierNotification: ");

    }

    private Notification getNotification(String contenu) {
        Notification.Builder builder = new Notification.Builder(this);
        builder.setContentTitle("Rappel");
        builder.setContentText(contenu);
        builder.setSmallIcon(R.mipmap.ic_launcher);
        return builder.build();
    }

    private String getContentNotification(String nom, int id, int jours) {
        Log.d(TAG, "getContentNotification: ");
        switch (id) {
            case 1:
                Log.d(TAG, "getContentNotification: classe");
                return "Votre classe: " + nom + " commence dans " + preferences.getString("preference_delais", "") + " minutes.";
            case 2:
                Log.d(TAG, "getContentNotification: examen");
                return "Votre examen: " + nom + "se déroule dans " + jours + " jours.";
            case 3:
                Log.d(TAG, "getContentNotification: devoir");
                return "Votre devoir: " + nom + " doit être remis dans " + jours + " jours.";
            default:
                return null;
        }
    }
}
