package net.info420.ritc.agendacegep;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;

//Gestionnaire des fragments qui permettent d'ajouter soit une classe, un devoir ou un examen

public class ModifierHoraireActivity extends FragmentActivity {
    private static final String TAG = "ModifierHoraireActivity";

    private SharedPreferences preferences;

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modifier_horaire);

        preferences = AgendaApplication.getPreferences();

        initToolbar();

        initUI();
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        toolbar.setTitle("");
        toolbar.setTitle(R.string.app_name);
        toolbar.setNavigationIcon(R.mipmap.ic_launcher);

        if (preferences.getString("theme_pref", "").equals("Rouge") || preferences.getString("theme_pref", "").equals("Red")) {
            toolbar.setBackgroundColor(getResources().getColor(R.color.rouge));
        } else {
            toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        }
    }

    private void initUI() {
        if (AgendaApplication.getAjouterClasse()) {
            AjouterClasse ajouterClasse = new AjouterClasse();

            ajouterClasse.setArguments(getIntent().getExtras());

            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            fragmentTransaction.add(R.id.fragmentContainer, ajouterClasse).commit();
        } else if (AgendaApplication.getAjouterExamen()) {
            AjouterExamen ajouterExamen = new AjouterExamen();

            ajouterExamen.setArguments(getIntent().getExtras());

            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            fragmentTransaction.add(R.id.fragmentContainer, ajouterExamen).commit();
        } else {
            AjouterDevoir ajouterDevoir = new AjouterDevoir();

            ajouterDevoir.setArguments(getIntent().getExtras());

            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            fragmentTransaction.add(R.id.fragmentContainer, ajouterDevoir).commit();
        }
    }
}
