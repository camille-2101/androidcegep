package net.info420.ritc.agendacegep;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

//Fragment utilisé lorsque l'on ajoute une classe dans l'horaire

public class AjouterClasse extends Fragment implements View.OnClickListener {
    Context context;

    private Calendar c = Calendar.getInstance();
    private static final String TAG = "AjouterClasse";
    private Button boutonAjouterClasse, boutonHeureDebut, boutonHeureFin, boutonDateDebut, boutonDateFin;
    private EditText editTextDateDebutClasse, editTextDateFinClasse, editTextHeureDebutClasse, editTextHeureFinClasse, editTextClasse, editTextProf;
    private int dateDebutJour = c.get(Calendar.DAY_OF_MONTH), dateDebutMois = c.get(Calendar.MONTH), dateDebutAnnee = c.get(Calendar.YEAR),
            heureFinHeure = c.get(Calendar.HOUR_OF_DAY), heureFinMinute = c.get(Calendar.MINUTE);
    private Spinner spinner;

    private SimpleDateFormat formatter;
    private SimpleDateFormat heureFormatter;

    public AjouterClasse() {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d(TAG, "onCreate: ");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;

        view = inflater.inflate(R.layout.fragment_ajouter_classe, container, false);

        boutonDateDebut = (Button) view.findViewById(R.id.boutonDateDebutClasse);
        boutonDateDebut.setOnClickListener(this);

        boutonDateFin = (Button) view.findViewById(R.id.boutonDateFinClasse);
        boutonDateFin.setOnClickListener(this);

        boutonAjouterClasse = (Button) view.findViewById(R.id.boutonAjouterClasse);
        boutonAjouterClasse.setOnClickListener(this);

        boutonHeureDebut = (Button) view.findViewById(R.id.boutonHeureDebutClasse);
        boutonHeureDebut.setOnClickListener(this);

        boutonHeureFin = (Button) view.findViewById(R.id.boutonHeureFinClasse);
        boutonHeureFin.setOnClickListener(this);

        editTextDateDebutClasse = (EditText) view.findViewById(R.id.editTextDateDebutCours);
        editTextDateDebutClasse.setKeyListener(null);

        editTextDateFinClasse = (EditText) view.findViewById(R.id.editTextDateFinCours);
        editTextDateFinClasse.setKeyListener(null);

        editTextHeureDebutClasse = (EditText) view.findViewById(R.id.editTextHeureDebutClasse);
        editTextHeureDebutClasse.setKeyListener(null);

        editTextHeureFinClasse = (EditText) view.findViewById(R.id.editTextHeureFinClasse);
        editTextHeureFinClasse.setKeyListener(null);

        editTextClasse = (EditText) view.findViewById(R.id.editTextNomClasse);

        editTextProf = (EditText) view.findViewById(R.id.editTextNomProfesseur);

        spinner = (Spinner) view.findViewById(R.id.spinnerJoursSemaine);

        Log.d(TAG, "onCreateView: ");

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        this.context = context;

        Log.d(TAG, "onAttach: ");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(TAG, "onDetach: ");
    }

    //Quand les boutons sont cliqués
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.boutonDateDebutClasse:
                DatePickerDialog debut = new DatePickerDialog(context,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                editTextDateDebutClasse.setText(String.format("%04d-%02d-%02d", year, monthOfYear + 1, dayOfMonth));
                            }
                        }, dateDebutAnnee, dateDebutMois, dateDebutJour);
                debut.show();
                break;
            case R.id.boutonDateFinClasse:
                DatePickerDialog fin = new DatePickerDialog(context,

                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                editTextDateFinClasse.setText(String.format("%04d-%02d-%02d", year, monthOfYear + 1, dayOfMonth));
                            }
                        }, dateDebutAnnee, dateDebutMois, dateDebutJour);
                fin.show();
                break;
            case R.id.boutonHeureDebutClasse:
                TimePickerDialog debut1 = new TimePickerDialog(context,
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                editTextHeureDebutClasse.setText(String.format("%02d:%02d", hourOfDay, minute));
                            }
                        }, heureFinHeure, heureFinMinute, true);
                debut1.show();
                break;
            case R.id.boutonHeureFinClasse:
                TimePickerDialog fin1 = new TimePickerDialog(context,
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                editTextHeureFinClasse.setText(String.format("%02d:%02d", hourOfDay, minute));
                            }
                        }, heureFinHeure, heureFinMinute, true);
                fin1.show();
                break;
            case R.id.boutonAjouterClasse:
                if (valider()){
                    ajouterClasse();
                    context.startService(new Intent(context, NotificationAgendaService.class));
                } else {
                    Toast.makeText(context, "Il y a une erreur. Vérifiez que tous vos champs soient remplis et que vos dates et heures soient logiques.",
                            Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    //Validation
    private boolean valider() {
        formatter = new SimpleDateFormat("yyyy-MM-dd");
        heureFormatter = new SimpleDateFormat("HH:mm");

        if (editTextClasse.getText().toString().isEmpty()){
            return false;
        }
        if (editTextProf.getText().toString().isEmpty()) {
            return false;
        }
        if (editTextDateDebutClasse.getText().toString().isEmpty()) {
            return false;
        }
        if (editTextDateFinClasse.getText().toString().isEmpty()) {
            return false;
        }
        if (editTextHeureDebutClasse.getText().toString().isEmpty()) {
            return false;
        }
        if (editTextHeureFinClasse.getText().toString().isEmpty()) {
            return  false;
        }

        try {
            Date dateDebut = formatter.parse(editTextDateDebutClasse.getText().toString());
            Date dateFin = formatter.parse(editTextDateFinClasse.getText().toString());

            Date heureDebut = heureFormatter.parse(editTextHeureDebutClasse.getText().toString());
            Date heureFin = heureFormatter.parse(editTextHeureFinClasse.getText().toString());

            if (dateDebut.after(dateFin)) {
                return false;
            }

            if(heureDebut.after(heureFin)){
                return false;
            }
        } catch (ParseException e) {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
        }
        return true;
    }

    //Requete faite au Content provider pour ajouer la classe
    private void ajouterClasse() {
        boolean erreur = false;

        ContentValues values = new ContentValues();
        values.put(Agenda.ClassEntry.COLUMN_CLASSE, editTextClasse.getText().toString());
        values.put(Agenda.ClassEntry.COLUMN_PROF, editTextProf.getText().toString());
        values.put(Agenda.ClassEntry.COLUMN_DATE_DEBUT, editTextDateDebutClasse.getText().toString());
        values.put(Agenda.ClassEntry.COLUMN_DATE_FIN, editTextDateFinClasse.getText().toString());
        values.put(Agenda.ClassEntry.COLUMN_HEURE_DEBUT, editTextHeureDebutClasse.getText().toString());
        values.put(Agenda.ClassEntry.COLUMN_HEURE_FIN, editTextHeureFinClasse.getText().toString());
        values.put(Agenda.ClassEntry.COLUMN_DAY, spinner.getSelectedItem().toString());
        String[] selection = {AgendaApplication.getUser(), AgendaApplication.getPassword()};
        String[] columns = {Agenda.UserEntry._ID};

        Cursor user = getContext().getContentResolver().query(Agenda.UserEntry.CONTENT_URI,
                columns,
                Agenda.UserEntry.COLUMN_USERNAME + " = ? AND " + Agenda.UserEntry.COLUMN_PASSWORD + " = ?",
                selection,
                null);

        if (user.moveToFirst())
            values.put(Agenda.ClassEntry.COLUMN_FK_USER, user.getString(user.getColumnIndex(Agenda.UserEntry._ID)));
        user.close();

        try {
            getContext().getContentResolver().insert(Agenda.ClassEntry.CONTENT_URI, values);
        } catch (SQLException e) {
            erreur = true;
        }

        if (erreur) {
            Toast.makeText(context, "Une erreur s'est produite. Echec de l'insertion de la classe dans la base de données.", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(context, "Insertion de la classe dans la base de données avec succès !", Toast.LENGTH_SHORT).show();
        }
    }
}
