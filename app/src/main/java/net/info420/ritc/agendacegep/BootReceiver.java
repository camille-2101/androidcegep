package net.info420.ritc.agendacegep;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

//Effectue l'action de démarrer le service de notification quand le BOOT est terminé

public class BootReceiver extends BroadcastReceiver {
    private static final String TAG = "BootReceiver";

    @Override
    public void onReceive(final Context context, Intent intent) {

        final Intent intent1 = new Intent(context, NotificationAgendaService.class);

        context.startService(intent1);

        Log.d(TAG, "onReceive: ");
    }
}
