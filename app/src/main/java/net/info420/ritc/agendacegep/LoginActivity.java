package net.info420.ritc.agendacegep;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

//Activité qui permet la connection utilisateur et l'enregistrement de ces informations dans les préférences

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "LoginActivity";

    private Toolbar toolbar;
    private Button boutonLogin, creerCompte;
    private EditText nomUtilisateur, motPasse;
    private PreferenceManager preferenceManager;
    private SharedPreferences preferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initToolbar();

        initUI();
    }

    private void initUI() {
        boutonLogin = (Button) findViewById(R.id.boutonLogin);
        nomUtilisateur = (EditText) findViewById(R.id.textUsername);
        motPasse = (EditText) findViewById(R.id.editTextPassword);
        creerCompte = (Button) findViewById(R.id.boutonCreerCompte);

        boutonLogin.setOnClickListener(this);
        creerCompte.setOnClickListener(this);
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        toolbar.setTitle(R.string.app_name);
        toolbar.setNavigationIcon(R.mipmap.ic_launcher);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.boutonLogin:
                if (userExists()) {
                    startActivity(new Intent(this, Accueil.class));
                } else {
                    Toast.makeText(this, "L'utilisateur n'existe pas.", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.boutonCreerCompte:
                startActivity(new Intent(this, CreerUtilisateur.class));
                break;
            default:
                break;
    }
}


    //Vérification de l'existence de l'utilisateur et enregistreemnt dans les préférences
    private boolean userExists() {
        String username = nomUtilisateur.getText().toString();
        String password = motPasse.getText().toString();
        String[] selection = {username, password};

        Cursor users = getApplicationContext().getContentResolver().query
                (Agenda.UserEntry.CONTENT_URI,
                        null,
                        Agenda.UserEntry.COLUMN_USERNAME + " = ? AND " + Agenda.UserEntry.COLUMN_PASSWORD + " = ?",
                        selection, null);

        if (users.getCount() != 0) {
            AgendaApplication.setUser(nomUtilisateur.getText().toString());
            AgendaApplication.setPassword(motPasse.getText().toString());

            users.close();
            return true;
        } else {
            users.close();
            return false;
        }
    }
}