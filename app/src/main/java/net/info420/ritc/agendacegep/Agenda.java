package net.info420.ritc.agendacegep;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by ritc on 2017-04-10.
 */

public class Agenda {

    //URI
    public static final String AUTHORITY = "net.info420.ritc.agenda.provider";

    private static final Uri BASE_CONTENT_URI = Uri.parse("content://" + AUTHORITY);

    public static final String PATH_USER = "user";
    public static final String PATH_CLASS = "class";
    public static final String PATH_DEVOIR = "devoir";
    public static final String PATH_EXAMEN = "examen";

    public static final class UserEntry implements BaseColumns {
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_USER).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_URI + "/" + PATH_USER;

        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_URI + "/" + PATH_USER;

        public static final String TABLE_NAME = "userTable";
        public static final String COLUMN_USERNAME = "username";
        public static final String COLUMN_PASSWORD = "password";

        public static Uri buildUserUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }


    //Pour la table CLASSE
    public static final class ClassEntry implements BaseColumns {
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_CLASS).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_URI + "/" + PATH_CLASS;

        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_URI + "/" + PATH_CLASS;

        public static final String TABLE_NAME = "classTable";
        public static final String COLUMN_CLASSE = "class";
        public static final String COLUMN_PROF = "professeur";
        public static final String COLUMN_DATE_DEBUT = "dateDebut";
        public static final String COLUMN_DATE_FIN = "dateFin";
        public static final String COLUMN_HEURE_DEBUT = "heureDebut";
        public static final String COLUMN_HEURE_FIN = "heureFin";
        public static final String COLUMN_DAY = "jourDeLaSemaine";
        public static final String COLUMN_FK_USER = "fk_user";

        public static Uri buildClassUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    //Pour la table DEVOIRS
    public static final class DevoirEntry implements BaseColumns {
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_DEVOIR).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_URI + "/" + PATH_DEVOIR;

        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_URI + "/" + PATH_DEVOIR;

        public static final String TABLE_NAME = "devoirTable";
        public static final String COLUMN_DEVOIR = "devoir";
        public static final String COLUMN_DESCRIPTION = "descripion";
        public static final String COLUMN_DATE_REMISE = "dateRemise";
        public static final String COLUMN_FK_USER = "fk_user";


        public static Uri buildDevoirUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    //Pour la table EXAMENS
    public static final class ExamenEntry implements BaseColumns {
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_EXAMEN).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_URI + "/" + PATH_EXAMEN;

        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_URI + "/" + PATH_EXAMEN;

        public static final String TABLE_NAME = "examenTable";
        public static final String COLUMN_EXAMEN = "matiere";
        public static final String COLUMN_PROFESSEUR = "professeur";
        public static final String COLUMN_HEURE_DEBUT = "heureDebut";
        public static final String COLUMN_HEURE_FIN = "heureFin";
        public static final String COLUMN_DATE = "date";
        public static final String COLUMN_FK_USER = "fk_user";

        public static Uri buildExamenUri (long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }
}
