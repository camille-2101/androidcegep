package net.info420.ritc.agendacegep;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

//CLasse pour les préférences

public class PrefActivity extends AppCompatActivity {
    private static final String TAG = "PrefActivity";
    private Toolbar toolbar;
    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pref_ui);

        preferences = AgendaApplication.getPreferences();

        initToolbar();

        getFragmentManager().beginTransaction().add(R.id.FragmentContainer,
                new PreferencesFragment()).commit();
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        toolbar.setTitle(R.string.app_name);
        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.couleurTitreToolbar));
        toolbar.setNavigationIcon(R.mipmap.ic_launcher);



        if (preferences.getString("theme_pref", "").equals("Rouge") || preferences.getString("theme_pref", "").equals("Red")) {
            toolbar.setBackgroundColor(getResources().getColor(R.color.rouge));
        } else {
            toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        }
    }


    public static class PreferencesFragment extends PreferenceFragment {

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_items);
        }
    }
}
